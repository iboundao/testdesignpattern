package org.example.service;

public interface PersonService {
    public int getTotalConfirm();
    public int getTotalVaccine();
    public int getTotalSousTraitement();
    public int getTotalGueris();
    public int getTotalDeces();
    public int getTotalEvacue();
}

package org.example.service.impl;

import org.example.repository.PersonRepository;
import org.example.service.PersonService;

public class PersonServiceImpl implements PersonService {
    public  final PersonRepository personRepository;

    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public int getTotalConfirm(){
        return personRepository.getTotalConfirm();
    }
    public int getTotalVaccine(){
        return personRepository.getTotalVaccine();
    }
    public int getTotalSousTraitement(){
        return personRepository.getTotalSousTraitement();
    }
    public int getTotalGueris(){
        return personRepository.getTotalGueris();
    }
    public int getTotalDeces(){
        return personRepository.getTotalDeces();
    }
    public int getTotalEvacue(){
        return personRepository.getTotalEvacue();
    }
}

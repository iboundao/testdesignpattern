package org.example.service.impl;

import org.example.repository.PersonRepository;
import org.example.repository.ZoneRepository;
import org.example.service.DisplayService;
import org.example.service.PersonService;
import org.example.service.ZoneService;

public class DisplayServiceImpl implements DisplayService {
    public final PersonService personService;
    public final ZoneService zoneService;

    public DisplayServiceImpl() {
        PersonRepository personRepository = new PersonRepository();
        PersonService personService = new PersonServiceImpl(personRepository) ;
        ZoneRepository zoneRepository = new ZoneRepository();
        ZoneService zoneService = new ZoneServiceImpl(zoneRepository);
        this.personService = personService;
        this.zoneService = zoneService;
    }

    public void displayDashboard() {
        System.out.println(String.format("1 - Total Confirmés         : %s", personService.getTotalConfirm()));
        System.out.println(String.format("2 - Total Vaccinés          : %s", personService.getTotalVaccine()));
        System.out.println(String.format("3 - Total Sous Traiement    : %s", personService.getTotalSousTraitement()));
        System.out.println(String.format("4 - Total Guéris            : %s", personService.getTotalGueris()));
        System.out.println(String.format("5 - Total Districts Touchés : %s", zoneService.getTotalDistrictTouche()));
        System.out.println(String.format("6 - Total Décès             : %s", personService.getTotalDeces()));
        System.out.println(String.format("7 - Total évacués           : %s", personService.getTotalEvacue()));
    }
}

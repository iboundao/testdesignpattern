package org.example.service.impl;

import org.example.repository.ZoneRepository;
import org.example.service.ZoneService;

public class ZoneServiceImpl implements ZoneService {
    public final ZoneRepository zoneRepository;

    public ZoneServiceImpl(ZoneRepository zoneRepository) {
        this.zoneRepository = zoneRepository;
    }

    public int getTotalDistrictTouche(){
        return zoneRepository.getTotalDistrictTouche();
    }
}

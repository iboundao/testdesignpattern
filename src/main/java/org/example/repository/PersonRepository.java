package org.example.repository;

public class PersonRepository {
    public int getTotalConfirm(){
        return 39836;
    }
    public int getTotalVaccine(){
        return 380665;
    }
    public int getTotalSousTraitement(){
        return 145;
    }
    public int getTotalGueris(){
        return 38668;
    }
    public int getTotalDeces(){
        return 1096;
    }
    public int getTotalEvacue(){
        return 1;
    }

}

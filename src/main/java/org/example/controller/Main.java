package org.example.controller;

import org.example.service.DisplayService;
import org.example.service.impl.DisplayServiceImpl;

public class Main {
    public void process(){
        DisplayService displayService =  new DisplayServiceImpl();
        System.out.println("************ Welcome **************");
        displayService.displayDashboard();
    }
}

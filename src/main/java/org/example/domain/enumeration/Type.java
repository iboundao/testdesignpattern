package org.example.domain.enumeration;

public enum Type {
    CONFIRME, VACCINE, SOUS_TRAITEMENT, GUERIS, TOUCHE, DECES, EVACUE;
}
